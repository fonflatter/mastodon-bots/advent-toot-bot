const https = require('https')
const Masto = require('mastodon')

const now = new Date()

const year = now.getFullYear() % 100
const month = now.getMonth() + 1
const day = now.getDate()

const isDecember = month === 12
const isAdvent = isDecember && (day < 25)

if (!isAdvent) {
  throw new Error('Es ist gar nicht Advent!')
}

const imageUrl = `https://www.fonflatter.de/adv${year}/${day}.png`
const text = `Heute öffnet sich das ${day}. Türchen im fetzigen Fredventskalender!

Sämtliche 24 Comicstrips des Fredventskalenders könnt ihr übrigens bestellen. Als Heft. Sendet mir einfach eine Mail an fonflatter@gmail.com.

https://www.fonflatter.de/der-fetzige-fredventskalender-2018/
`

const mastodonSecrets = process.env.MASTODON_SECRETS
if (!mastodonSecrets) {
  throw new Error('MASTODON_SECRETS ist nicht definiert!')
}

const mastodonClient = new Masto(JSON.parse(mastodonSecrets))

uploadImage()
  .then(sendToot)

function sendToot (imageId) {
  return mastodonClient.post('statuses', {
    spoiler_text: 'Comic / Adventskalender',
    status: text,
    media_ids: [imageId]
  })
    .then(({ data: toot }) => {
      console.log({
        tootUrl: toot.uri,
        text
      })
    })
}

function uploadImage () {
  return new Promise(resolve => https.get(imageUrl, (file) => resolve(file)))
    .then(file => mastodonClient.post('media', { file }))
    .then(response => response.data.id)
}
